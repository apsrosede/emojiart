//
//  ImagePicker.swift
//  EmojiArt
//
//  Created by user178036 on 8/23/20.
//  Copyright © 2020 apsrose. All rights reserved.
//

import SwiftUI
import UIKit

typealias PickedImageHandler =  (UIImage?) -> Void

struct ImagePicker: UIViewControllerRepresentable {
    var handlePickedImage: PickedImageHandler
    
    func makeUIViewController(context: Context) ->  UIImagePickerController {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = context.coordinator
        return picker
    }
    
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(handlePickedImage: handlePickedImage)
    }
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var handlePickedImage: (UIImage?) -> Void
        
        init(handlePickedImage: @escaping (UIImage?) -> Void) {
            self.handlePickedImage = handlePickedImage
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            handlePickedImage(nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            handlePickedImage(info[.originalImage] as? UIImage)
            
        }
        
    }
    
}
