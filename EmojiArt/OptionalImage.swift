//
//  OptionalImage.swift
//  EmojiArt
//
//  Created by AdrianRoseMacBookAir on 31.07.20.
//  Copyright © 2020 apsrose. All rights reserved.
//

import SwiftUI

struct OptionalImage : View {
    
    var uiImage: UIImage?
    
    var body: some View {
        Group {
            if self.uiImage != nil {
                Image(uiImage: self.uiImage!)
            }
        }
    }
    
}

struct OptionalImage_Previews: PreviewProvider {
    static var previews: some View {
        OptionalImage()
    }
}

